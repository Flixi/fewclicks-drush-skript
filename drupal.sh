#!/bin/bash
#Set Location where Drupal should be installed to
echo Enter location where the drupal folder should be copied to
read DRUPALDESTINATION
#Download newest Core Version into chosen destination
drush -y dl --destination=$DRUPALDESTINATION

#Get MySQL Username
echo Please enter your mysql username
read MYSQLUSER
#Get MySQL Password
echo Please enter your mysql password
read MYSQLPASSWORD
#Get MySQL Database
echo Enter the name of the database
read MYSQLDATABASE

#Get absolute Path to Skript
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
#Move all files from Drupal-* folder to chosen destination
mv $DIR/$DRUPALDESTINATION/drupal*/* $DIR/$DRUPALDESTINATION
mv $DIR/$DRUPALDESTINATION/drupal*/.htaccess $DIR/$DRUPALDESTINATION
cd $DRUPALDESTINATION

#Ask for Drupal User
echo Please set your Drupal User Account
read DRUPALUSER
echo Please set your Drupal User Password
read DRUPALPASS

#Installs Drupal with chosen database parameters
drush -y si standard --db-url=mysql://$MYSQLUSER:$MYSQLPASSWORD@127.0.0.1/$MYSQLDATABASE --account-name=$DRUPALUSER --account-pass=$DRUPALPASS

#Chaos tool
drush dl ctools && drush en ctools -y
#Admin Menu
drush dl admin_menu && drush en admin_menu -y
drush dis menu -y #Standard Admin Menü ausschalten
#Entity Api
drush dl entity -y
drush en entity -y
drush en entity_token -y
#Backup & Migrate
drush dl backup_migrate && drush en backup_migrate -y
#Date
drush dl date
#Devel
drush dl devel && drush en devel -y
#Libraries
drush dl libraries && drush en libraries -y
#Token
drush dl token && drush en token -y
#Metatag
drush dl metatag
drush en metatag -y
drush en metatag_ui -y
drush en metatag_views -y
drush en metatag_context -y
#Pathauto
drush dl pathauto && drush en pathauto -y
#Views
drush dl views 
drush en views -y
drush en views_ui -y
#XML Sitemap
drush dl xmlsitemap
drush en xmlsitemap -y
drush en xmlsitemap_engines -y
drush en xmlsitemap_node -y
drush en xmlsitemap_taxonomy -y

#Ask user if Commerce Modules should be installed
while true; do
    read -p "Do you want to install Commerce? (Y/N)" answer
    case $answer in
        [Yy]* ) echo "YES"; 
			    drush dl rules;
			    drush en rules -y;
			    drush en rules_admin -y;
			    drush en rules_scheduler -y;

			    drush dl commerce;
			    drush en commerce -y; 
			    drush en commerce_cart -y;
			    drush en commerce_checkout -y;
			    drush en commerce_customer -y;
			    drush en commerce_line_item -y;
			    drush en commerce_order -y;
			    drush en commerce_payment -y;
			    drush dis commerce_payment_example -y;
			    drush en commerce_price -y;
			    drush en commerce_product -y;
			    drush en commerce_product_pricing -y;
			    drush en commerce_product_reference -y;
			    drush en commerce_tax -y;

			    drush dl addressfield;
			    drush en addressfield -y;

			    drush dl inline_entity_form;
			    drush en inline_entity_form -y ;

			    drush dl commerce_currency_settings;
			    drush en commerce_currency_settings -y;

			    drush dl commerce_stock;
			    drush en commerce_stock -y ;

			    drush dl references;
			    drush en references -y;

			    drush dl commerce_shipping;
			    drush en commerce_shipping -y;

			    drush dl commerce_ogone;
			    drush en commerce_ogone -y;
				break;;
        [Nn]* ) echo "NO"; break;;
        * ) echo "Please answer y or n.";;
    esac
done

#Ask user if internalisation should be installed
while true; do
    read -p "Install Internationalization? (Y/N)" answer
    case $answer in
        [Yy]* ) echo "YES"; 
				drush dl i18n;
				drush en i18n -y;

				drush dl transliteration;
				drush en transliteration -y;

				drush dl variable;
				drush en variable -y;

				drush dl l10n_update;
				drush en l10n_update -y;

				drush dl admin_language;
				drush en admin_language -y;

				drush dl languageicons;
				drush en languageicons -y;
				break;;
        [Nn]* ) echo "NO"; break;;
        * ) echo "Please answer y or n.";;
    esac
done

#Get name of new theme
echo Name des Themes:
read THEMENAME
#Create folders for new theme
mkdir sites/all/themes/$THEMENAME
mkdir sites/all/themes/$THEMENAME/templates
mkdir sites/all/themes/$THEMENAME/css
mkdir sites/all/themes/$THEMENAME/js
#Copies page.tpl.php to new theme
cp modules/system/page.tpl.php sites/all/themes/$THEMENAME/templates/page.tpl.php
#Create dummy files for new theme
cd sites/all/themes/$THEMENAME
touch $THEMENAME.info
touch css/styles.css
touch css/ie.css
touch js/$THEMENAME.js
#Fill .info file
echo "name = $THEMENAME" >> $THEMENAME.info
echo "description = New website theme" >> $THEMENAME.info
echo "screenshot = screen.jpg" >> $THEMENAME.info
echo "package = Core" >> $THEMENAME.info
echo "version = VERSION" >> $THEMENAME.info
echo "core = 7.x" >> $THEMENAME.info
echo " " >> $THEMENAME.info
echo "stylesheets[all][] = css/styles.css" >> $THEMENAME.info
echo "; stylesheets-conditional[IE][all][] = css/ie.css" >> $THEMENAME.info 
echo "scripts[] = js/$THEMENAME.js" >> $THEMENAME.info
echo " " >> $THEMENAME.info
echo "regions[header] = Header" >> $THEMENAME.info
echo "regions[slider] = Slider" >> $THEMENAME.info
echo "regions[slogan] = Slogan" >> $THEMENAME.info
echo "regions[content] = content" >> $THEMENAME.info
echo "regions[footer] = footer" >> $THEMENAME.info
echo " " >> $THEMENAME.info
echo "regions[highlighted] = Highlighted" >> $THEMENAME.info
echo "regions[help] = Help" >> $THEMENAME.info

#Installation complete
echo Installation completed!
echo Username: $DRUPALUSER
echo Password: $DRUPALPASS